#### About

Simple note-taking web app that supports notes, todos, markdown, tags, folders and encrypted folders.
To take a quick look use login and password (test, test) here https://app.mirari.io/login


#### Build and run locally
```bash
./gradlew build
java -jar core/build/libs/core.jar
```
