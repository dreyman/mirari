module.exports = {
    semi: true,
    singleQuote: true,
    printWidth: 100,
    svelteSortOrder: 'scripts-markup-styles',
    plugins: ['svelte']
};
