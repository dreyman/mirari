import axios from 'axios';
import Api from './api';
import { notes, folders } from './stores';

const api = new Api({
  token: localStorage.getItem('access'),
  refreshToken: localStorage.getItem('refresh'),
  saveToken,
  failedRefresh() {
    localStorage.removeItem('access');
    localStorage.removeItem('refresh');
    window.location.href = '/';
  }
});

export const getNotes = () => {
  return api.getNotes();
}

export const createNote = async (note) => {
  note = await api.createNote(note);
  notes.add(note);
  return note;
}

export const createFolder = async (folder) => {
  const f = await api.createNote(folder);
  folders.add(f);
  return f;
}

export const removeNote = async (id) => {
  await api.removeNote(id);
  notes.remove(id);
}

export const removeFolder = async (id, removeNotes = false) => {
  return api.removeFolder(id, removeNotes);
}

export const updateNote = async (note) => {
  return await api.updateNote(note);
}

export const updateProps = async (id, props) => {
  return await api.updateProps(id, props);
}

export const toggleDone = async (note) => {
  note.props.todo.done = !note.props.todo.done;
  await api.updateProps(note.id, note.props);
  notes.update(note);
}

export const login = async (username, password) => {
  const { data } = await axios.post("/auth/login", { username, password });
  saveToken(data);
  return data;
}
export const signup = async (username, password) => {
  const { data } = await axios.post("/auth/signup", { username, password });
  saveToken(data);
  return data;
}

export const logout = async () => {
  await api.logout();
  localStorage.removeItem('access');
  localStorage.removeItem('refresh');
}

function saveToken(data) {
  localStorage.setItem('access', data.accessToken);
  localStorage.setItem('refresh', data.refreshToken);
}
