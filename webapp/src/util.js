import CryptoJS from 'crypto-js';

export const encrypt = function (text, key) {
    return CryptoJS.AES.encrypt(text, key).toString();
}

export const decrypt = function (text, key) {
    return CryptoJS.AES.decrypt(text, key).toString(CryptoJS.enc.Utf8);
}
