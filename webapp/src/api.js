import axios from "axios";

export default class Api {
  constructor(options = {}) {
    this.client = options.client || axios.create();
    this.token = options.token;
    this.refreshToken = options.refreshToken;
    this.refreshRequest = null;

    this.client.interceptors.request.use(
      config => {
        if (!this.token) {
          return config;
        }

        const newConfig = {
          headers: {},
          ...config,
        };

        newConfig.headers.Authorization = `Bearer ${this.token}`;
        return newConfig;
      },
      e => Promise.reject(e)
    );

    this.client.interceptors.response.use(
      r => r,
      async error => {
        if (error.response.config.url === '/auth/refresh' && error.response.status === 404 && options.failedRefresh) {
          options.failedRefresh();
        }

        if (!this.refreshToken || error.response.status !== 403 || error.config.retry)
          throw error;

        const { data } = await this.client.post("/auth/refresh", {
          refreshToken: this.refreshToken,
        });
        this.token = data.accessToken;
        this.refreshToken = data.refreshToken;
        if (options.saveToken) options.saveToken(data);

        const newRequest = {
          ...error.config,
          retry: true,
        };

        return this.client(newRequest);
      }
    );
  }

  async logout() {
    await this.client.post('/auth/logout');
    this.token = null;
    this.refreshToken = null;
  }

  async getNotes() {
    const { data } = await this.client('/api/notes');
    return data;
  }

  removeNote(id) {
    return this.client.delete('/api/notes/' + id);
  }

  async createNote(note) {
    const props = JSON.stringify(note.props);
    const { data } = await this.client.post('/api/notes/new', { ...note, props });
    return data;
  }

  async updateNote(note) {
    const props = JSON.stringify(note.props);
    const { data } = await this.client.put(`/api/notes/${note.id}`, { ...note, props });
    return data;
  }

  updateProps(noteId, props) {
    return this.client.put(`/api/notes/${noteId}/props`, { ...props });
  }

  removeFolder(folderId, removeNotes = false) {
    const params = removeNotes ? '?deleteNotes=true' : '?deleteNotes=false';
    return this.client.delete(`/api/folders/${folderId}${params}`);
  }
}
