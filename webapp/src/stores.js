import { writable, derived } from 'svelte/store';

function createNotesStore() {
  const { subscribe, set, update } = writable([]);

  return {
    subscribe,
    set: notes => set(notes),
    add: note => {
      update(notes => [note, ...notes])
    },
    remove: id => update(notes => {
      const idx = notes.findIndex(n => n.id === id);
      if (idx != -1)
        notes.splice(idx, 1);
      return notes;
    }),
    update: note => update(notes => {
      const idx = notes.findIndex(n => n.id === note.id);
      if (idx != -1) {
        notes[idx] = note;
      }
      return notes;
    })
  };
}

function createFoldersStore() {
  const { subscribe, set, update } = writable([]);

  return {
    subscribe,
    set: folders => set(folders),
    add: folder => update(folders => [folder, ...folders]),
    remove: id => update(folders => {
      const idx = folders.findIndex(f => f.id === id);
      if (idx != -1)
        folders.splice(idx, 1);
      return folders;
    }),
    update: folder => update(folders => {
      const idx = folders.findIndex(f => f.id === folder.id);
      if (idx != -1)
        folders[idx] = folder;
      return folders;
    })
  };
}

function createTagsStore() {
  return derived(notes, $notes => {
    const map = $notes.flatMap(n => n.props.tags).reduce((map, tag) => map.set(tag, (map.get(tag) || 0) + 1), new Map());
    const tags = [];
    for (let [tag, count] of map) {
      tags.push({ name: tag, count });
    }
    tags.sort((t1, t2) => t2.count - t1.count);
    return tags;
  });
}

export const notes = createNotesStore();
export const folders = createFoldersStore();
export const tags = createTagsStore();
export const selectedTag = writable(null);
