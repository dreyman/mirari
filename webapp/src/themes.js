const root = document.documentElement;

const DEFAULT_COLOR = '23, 191, 99';

export const initTheme = function () {
    const userTheme = localStorage.getItem('theme');
    const userColor = localStorage.getItem('color');
    if (userTheme && userTheme === 'light') {
        light();
    } else {
        dark();
    }
    if (userColor) setColor(userColor);
    else setColor(DEFAULT_COLOR);
}

export const setColor = function (color, save = false) {
    if (save)
        localStorage.setItem('color', color);

    root.style.setProperty('--primary-color', 'rgb(' + color + ')');
    root.style.setProperty('--primary-color-rgb', color);
}

export const dark = function (save = false) {
    if (save)
        localStorage.setItem('theme', 'dark');

    root.style.setProperty('--bg-color', '#15202b');
    root.style.setProperty('--box-bg', '#192734');
    root.style.setProperty('--text-color', 'white');
    root.style.setProperty('--input-bg', '#192734');
    root.style.setProperty('--input-focus-border', 'white');
    root.style.setProperty('--navlink-inactive-text', 'white');
    root.style.setProperty('--navlink-hover-bg', '#143030');
    root.style.setProperty('--border-color', 'rgb(56, 68, 77)');
    root.style.setProperty('--lightgray', 'rgb(136, 153, 166)');
    root.style.setProperty('--note-hover-bg', '#21303d');
    root.style.setProperty('--dropdown-bg', '#15202b');
    root.style.setProperty('--dropdown-option-hover-bg', 'rgba(var(--primary-color-rgb), 0.4)');
}

export const light = function (save = false) {
    if (save)
        localStorage.setItem('theme', 'light');


    root.style.setProperty('--bg-color', 'white');
    root.style.setProperty('--box-bg', '#f5f8fa');
    root.style.setProperty('--text-color', 'black');
    root.style.setProperty('--input-bg', '#f5f8fa');
    root.style.setProperty('--input-focus-border', 'gray');
    root.style.setProperty('--navlink-inactive-text', 'black');
    root.style.setProperty('--navlink-hover-bg', '#e7f9ef');
    root.style.setProperty('--border-color', 'rgb(136, 153, 166)');
    root.style.setProperty('--lightgray', 'rgb(136, 153, 166)');
    root.style.setProperty('--note-hover-bg', '#edf2f5');
    root.style.setProperty('--dropdown-bg', 'white');
    root.style.setProperty('--dropdown-option-hover-bg', 'rgba(var(--primary-color-rgb), 0.4)');
}
