import App from './App.svelte';
import Login from './Login.svelte';
import * as theme from './themes';

theme.initTheme();

const token = localStorage.getItem('refresh');

let app;

if (token) {
	app = new App({
		target: document.body
	});
} else {
	app = new Login({
		target: document.body
	});
}

export default app;
