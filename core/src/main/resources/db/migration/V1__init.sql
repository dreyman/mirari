CREATE TABLE users (
    id bigserial PRIMARY KEY,
    username varchar(30) UNIQUE,
    password varchar(120),
    created_at timestamp
);

CREATE TABLE note (
    id bigserial PRIMARY KEY,
    user_id bigint references users(id),
    content text,
    attrs jsonb,
    created_at timestamp
);

CREATE TABLE token (
    user_id bigint references users(id),
    token varchar(50) UNIQUE,
    expiry_date date
);
