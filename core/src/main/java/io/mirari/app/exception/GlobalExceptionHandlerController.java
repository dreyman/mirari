package io.mirari.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
public class GlobalExceptionHandlerController {

    @ExceptionHandler(AuthException.class)
    public void handleCustomException(HttpServletResponse resp, AuthException ex) {
        resp.setStatus(ex.getHttpStatus().value());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public void handleAccessDeniedException(HttpServletResponse resp) {
        resp.setStatus(HttpStatus.UNAUTHORIZED.value());
    }

    @ExceptionHandler(NotFoundException.class)
    public void handleNotFoundException(HttpServletResponse resp) {
        resp.setStatus(HttpStatus.NOT_FOUND.value());
    }

}
