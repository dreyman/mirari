package io.mirari.app.model;

import java.time.LocalDateTime;

public class Note {

    public long id;
    public long userId;
    public String content;
    public String props;
    public LocalDateTime createdAt;

    public Note() {}

    public Note(long id, long userId, String content, String props, LocalDateTime createdAt) {
        this.id = id;
        this.userId = userId;
        this.content = content;
        this.props = props;
        this.createdAt = createdAt;
    }
}
