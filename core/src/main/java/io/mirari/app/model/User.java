package io.mirari.app.model;

import java.time.LocalDateTime;

public class User {

    public long id;
    public String username;
    public String password;
    public LocalDateTime createdAt;

    public User() {}

    public User(long id, String username, String password, LocalDateTime createdAt) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.createdAt = createdAt;
    }
}
