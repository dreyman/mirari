package io.mirari.app.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtTokenFilter extends OncePerRequestFilter {

  private JwtTokenProvider tokenProvider;

  public JwtTokenFilter(JwtTokenProvider tokenProvider) {
    this.tokenProvider = tokenProvider;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                  HttpServletResponse httpServletResponse,
                                  FilterChain filterChain) throws ServletException, IOException {
    String jwt = resolveToken(httpServletRequest);

    if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
      Authentication authentication = tokenProvider.getAuthentication(jwt);
      SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    filterChain.doFilter(httpServletRequest, httpServletResponse);
  }

  private String resolveToken(HttpServletRequest request) {
    String apiKey = request.getParameter("api_key");
    String bearerToken = request.getHeader("Authorization");
    boolean isValidBearerToken = StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ");
    boolean isValidApiKey = StringUtils.hasText(apiKey);
    if (isValidBearerToken) {
      return bearerToken.substring(7);
    } else if (isValidApiKey) {
      return apiKey;
    }
    return null;
  }

}
