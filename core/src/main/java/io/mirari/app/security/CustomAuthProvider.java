package io.mirari.app.security;

import io.mirari.app.model.User;
import io.mirari.app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthProvider implements AuthenticationProvider {

    @Autowired UserRepository userRepository;
    @Autowired PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (authentication instanceof UsernamePasswordAuthenticationToken) {
            var credentials = (UsernamePasswordAuthenticationToken) authentication;

            User user = userRepository.findByUsername(credentials.getName());

            if (user == null || !passwordEncoder.matches(credentials.getCredentials().toString(), user.password))
                throw new BadCredentialsException("Incorrect username or password");

            return new CustomAuthToken(user.id);
        }
        return authentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication == CustomAuthToken.class || authentication == UsernamePasswordAuthenticationToken.class;
    }
}
