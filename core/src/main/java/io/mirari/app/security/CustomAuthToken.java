package io.mirari.app.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;

import java.util.Collections;

public class CustomAuthToken extends AbstractAuthenticationToken {

    private final long userId;

    public CustomAuthToken(long userId) {
        super(Collections.emptyList());
        this.userId = userId;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Long getPrincipal() {
        return userId;
    }
}
