package io.mirari.app.controller.dto;

public class RefreshTokenRequest {

    public String refreshToken;

    public RefreshTokenRequest() {}

    public RefreshTokenRequest(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
