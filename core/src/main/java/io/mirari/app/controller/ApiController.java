package io.mirari.app.controller;

import io.mirari.app.controller.dto.NoteDto;
import io.mirari.app.controller.dto.UserDto;
import io.mirari.app.model.Note;
import io.mirari.app.model.User;
import io.mirari.app.service.NoteService;
import io.mirari.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired NoteService noteService;
    @Autowired UserService userService;

    @GetMapping("hi")
    public String hi() {
        return "yo";
    }

    @GetMapping("me")
    public ResponseEntity<UserDto> me(Authentication auth) {
        long userId = (long) auth.getPrincipal();
        User user = userService.getUser(userId);
        return ResponseEntity.ok(new UserDto(user));
    }

    @GetMapping("notes")
    public ResponseEntity<List<NoteDto>> all(Authentication auth) {
        long userId = (long) auth.getPrincipal();
        var notes = noteService.all(userId);
        var dto = notes.stream()
                .map(NoteDto::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok(dto);
    }

    @PostMapping("notes/new")
    public ResponseEntity<NoteDto> create(@RequestBody NoteDto noteDto, Authentication auth) {
        Note note = noteDto.getNote();
        note.userId = (long) auth.getPrincipal();
        noteService.save(note);
        return ResponseEntity.ok(new NoteDto(note));
    }

    @PutMapping("notes/{id}")
    public ResponseEntity<NoteDto> update(@RequestBody NoteDto noteDto, Authentication auth) {
        long user = (long) auth.getPrincipal();
        Note note = noteDto.getNote();
        noteService.update(note, user);
        return ResponseEntity.ok(noteDto);
    }

    @PutMapping("notes/{noteId}/props")
    public ResponseEntity<String> updateProps(@PathVariable long noteId, @RequestBody String newProps, Authentication auth) {
        long user = (long) auth.getPrincipal();
        noteService.updateProps(noteId, newProps, user);
        return ResponseEntity.ok(newProps);
    }

    @DeleteMapping("notes/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id, Authentication auth) {
        long user = (long) auth.getPrincipal();
        noteService.delete(id, user);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("folders/{id}")
    public ResponseEntity<?> deleteFolder(@PathVariable Long id,
                                          @RequestParam(name = "deleteNotes", required = false) boolean deleteNotes,
                                          Authentication auth) {
        long user = (long) auth.getPrincipal();

        if (deleteNotes)
            noteService.deleteFolderAndNotes(id, user);
        else
            noteService.deleteFolder(id, user);

        return ResponseEntity.ok().build();
    }
}
