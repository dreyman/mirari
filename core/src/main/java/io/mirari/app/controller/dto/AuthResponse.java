package io.mirari.app.controller.dto;

public class AuthResponse {
    public String accessToken;
    public String refreshToken;

    public AuthResponse() {}

    public AuthResponse(String accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }
}
