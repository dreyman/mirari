package io.mirari.app.controller.dto;

import io.mirari.app.model.User;

import java.time.ZoneOffset;

public class UserDto {

    public long id;
    public String username;
    public long createdAt;

    public UserDto(User user) {
        id = user.id;
        username = user.username;
        createdAt = user.createdAt.toInstant(ZoneOffset.UTC).toEpochMilli();
    }
}
