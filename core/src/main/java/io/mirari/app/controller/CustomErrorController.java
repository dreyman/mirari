package io.mirari.app.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class CustomErrorController implements ErrorController {

    @RequestMapping("/error")
    public String handleErrorGet(HttpServletRequest req, HttpServletResponse resp) {
        if (resp.getStatus() == 404)
            resp.setStatus(200);
        return "forward:/";
    }

    @RequestMapping(value = "/error", method = {RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.POST})
    public HttpServletResponse handleError(HttpServletRequest req, HttpServletResponse resp) {
        return resp;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
