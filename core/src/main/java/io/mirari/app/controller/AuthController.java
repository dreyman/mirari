package io.mirari.app.controller;

import io.mirari.app.auth.AuthRepository;
import io.mirari.app.auth.AuthService;
import io.mirari.app.controller.dto.AuthResponse;
import io.mirari.app.controller.dto.LoginRequest;
import io.mirari.app.controller.dto.MessageResponse;
import io.mirari.app.controller.dto.RefreshTokenRequest;
import io.mirari.app.model.User;
import io.mirari.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired AuthService auth;
    @Autowired UserService userService;
    @Autowired AuthenticationManager authenticationManager;

    @PostMapping("login")
    public ResponseEntity<?> login(@RequestBody LoginRequest req) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(req.username, req.password));

            User user = userService.getUser(req.username);

            Map.Entry<String, String> token = auth.createToken(user.id);
            return ok(new AuthResponse(token.getKey(), token.getValue()));
        } catch (AuthenticationException x) {
            return status(401).body(new MessageResponse("Incorrect username or password"));
        }
    }

    @PostMapping("signup")
    public ResponseEntity<?> signup(@RequestBody LoginRequest req) {
        User user = userService.create(req.username, req.password);

        if (user == null)
            return status(422).body(new MessageResponse("Username is already in use"));

        Map.Entry<String, String> token = auth.createToken(user.id);
        return ok(new AuthResponse(token.getKey(), token.getValue()));
    }

    @PostMapping("refresh")
    public ResponseEntity<?> refresh(@RequestBody RefreshTokenRequest req) {
        Map.Entry<String, String> token = auth.refresh(req.refreshToken);
        if (token == null)
            return ResponseEntity.notFound().build();
        else
            return ResponseEntity.ok(new AuthResponse(token.getKey(), token.getValue()));
    }

    @PostMapping("logout")
    public ResponseEntity<?> logout(Authentication authentication) {
        long userId = (long) authentication.getPrincipal();
        auth.logout(userId);
        return ResponseEntity.ok().build();
    }
}
