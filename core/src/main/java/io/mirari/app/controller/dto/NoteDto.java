package io.mirari.app.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonRawValue;
import io.mirari.app.model.Note;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class NoteDto {

    public long id;
    public long userId;
    public String content;
    @JsonRawValue
    public String props;
    public long createdAt;

    public NoteDto() {}

    public NoteDto(Note note) {
        id = note.id;
        userId = note.userId;
        content = note.content;
        props = note.props;
        createdAt = note.createdAt.toInstant(ZoneOffset.UTC).toEpochMilli();
    }

    @JsonIgnore
    public Note getNote() {
        return new Note(id, userId, content, props, createdAt != 0 ? LocalDateTime.ofInstant(Instant.ofEpochMilli(createdAt), ZoneOffset.UTC) : null);
    }
}
