package io.mirari.app.service;

import io.mirari.app.model.User;

public interface UserService {

    User getUser(String username);

    User getUser(long id);

    User create(String username, String password);
}
