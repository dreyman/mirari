package io.mirari.app.service;

import io.mirari.app.model.Note;

import java.util.List;

public interface NoteService {

    List<Note> all(long userId);

    void save(Note note);

    void update(Note note, long userId);

    void updateProps(long noteId, String props, long userId);

    void delete(long id, long userId);

    void deleteFolder(long folderId, long userId);

    void deleteFolderAndNotes(long folderId, long userId);
}
