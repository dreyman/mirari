package io.mirari.app.service;

import io.mirari.app.model.User;
import io.mirari.app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class UserServiceImpl implements UserService {

    @Autowired PasswordEncoder passwordEncoder;
    @Autowired UserRepository userRepository;

    @Override
    public User getUser(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User getUser(long id) {
        return userRepository.findById(id);
    }

    @Override
    public User create(String username, String password) {
        if (!userRepository.existsByUsername(username)) {
            User user = new User();
            user.password = passwordEncoder.encode(password);
            user.username = username;
            user.createdAt = LocalDateTime.now(ZoneOffset.UTC);
            user.id = userRepository.save(user);
            return user;
        } else {
            return null;
        }
    }
}
