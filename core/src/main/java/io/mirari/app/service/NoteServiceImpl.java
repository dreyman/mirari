package io.mirari.app.service;

import io.mirari.app.exception.NotFoundException;
import io.mirari.app.model.Note;
import io.mirari.app.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {

    NoteRepository repo;

    @Autowired
    public NoteServiceImpl(NoteRepository noteRepo) {
        repo = noteRepo;
    }

    public List<Note> all(long userId) {
        return repo.findAll(userId);
    }

    public void save(Note note) {
        note.createdAt = LocalDateTime.now(ZoneOffset.UTC);
        note.id = repo.save(note);
    }

    @Override
    public void update(Note note, long userid) {
        int updatedRows = repo.update(note, userid);
        if (updatedRows == 0)
            throw new NotFoundException(String.format("Note with id %d (user: %d) not found", note.id, userid));

    }

    @Override
    public void updateProps(long noteId, String props, long userId) {
        int updatedRows = repo.updateProps(noteId, props, userId);
        if (updatedRows == 0)
            throw new NotFoundException(String.format("Note with id %d (user: %d) not found", noteId, userId));
    }

    public void delete(long id, long userid) {
        int deletedRows = repo.delete(id, userid);
        if (deletedRows == 0)
            throw new NotFoundException(String.format("Note with id %d (user: %d) not found", id, userid));
    }

    @Override
    @Transactional
    public void deleteFolder(long folderId, long userId) {
        int deletedRows = repo.delete(folderId, userId);
        if (deletedRows == 0)
            throw new NotFoundException(String.format("Folder with id %d (user: %d) not found", folderId, userId));
        repo.clearFolder(folderId, userId);
    }

    @Override
    @Transactional
    public void deleteFolderAndNotes(long folderId, long userId) {
        int deletedRows = repo.delete(folderId, userId);
        if (deletedRows == 0)
            throw new NotFoundException(String.format("Folder with id %d (user: %d) not found", folderId, userId));
        repo.deleteByFolder(folderId, userId);
    }
}
