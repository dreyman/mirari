package io.mirari.app.auth;

import java.time.LocalDate;

public interface AuthRepository {

    RefreshToken find(String token);

    void save(RefreshToken refreshToken);

    void refresh(String oldToken, String newToken, LocalDate expiryDate);

    void remove(long userId);

    void removeExpiredTokens();
}
