package io.mirari.app.auth;

import java.util.Map;

public interface AuthService {
    Map.Entry<String, String> createToken(long userId);

    Map.Entry<String, String> refresh(String refreshToken);

    void logout(long userId);
}
