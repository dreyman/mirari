package io.mirari.app.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@Repository
public class AuthRepositoryImpl implements AuthRepository {

    @Autowired JdbcTemplate jdbc;
    RowMapper<RefreshToken> rowMapper = new TokenRowMapper();

    @Override
    public RefreshToken find(String token) {
        var q = "SELECT * FROM token WHERE token = ?";
        List<RefreshToken> res = jdbc.query(q, rowMapper, token);
        return res.isEmpty() ? null : res.get(0);
    }

    @Override
    public void save(RefreshToken t) {
        var q = "INSERT INTO token (user_id, token, expiry_date) values (?, ?, ?)";
        jdbc.update(q, t.userId, t.token, t.expiryDate);
    }

    @Override
    public void refresh(String oldToken, String newToken, LocalDate expiryDate) {
        var q = "UPDATE token SET token = ?, expiry_date = ? WHERE token = ?";
        jdbc.update(q, newToken, expiryDate, oldToken);
    }

    @Override
    public void remove(long userId) {
        var q = "DELETE FROM token WHERE user_id = ?";
        jdbc.update(q, userId);
    }

    @Override
    public void removeExpiredTokens() {
        var q = "DELETE FROM token WHERE expiry_date < CURRENT_DATE";
        jdbc.update(q);
    }

    static class TokenRowMapper implements RowMapper<RefreshToken> {

        @Override
        public RefreshToken mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new RefreshToken(rs.getLong("user_id"),
                    rs.getString("token"),
                    rs.getDate("expiry_date").toLocalDate());
        }
    }
}
