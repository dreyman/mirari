package io.mirari.app.auth;

import io.mirari.app.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Random;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired AuthRepository authRepository;
    @Autowired JwtTokenProvider jwtTokenProvider;

    @Value("${security.jwt.refresh-token.expire-in-days}")
    private int refreshTokenExpiry;

    private TokenGenerator tokenGenerator = new TokenGenerator();

    @Scheduled(cron = "* 0 1 * * *")
    public void removeExpiredTokens() {
        authRepository.removeExpiredTokens();
    }

    @Override
    public Map.Entry<String, String> createToken(long userId) {
        String accessToken = jwtTokenProvider.createToken(userId);
        String refreshToken = tokenGenerator.generate();

        authRepository.save(new RefreshToken(userId, refreshToken, LocalDate.now().plusDays(refreshTokenExpiry)));

        return new AbstractMap.SimpleEntry<>(accessToken, refreshToken);
    }

    public Map.Entry<String, String> refresh(String refreshToken) {
        RefreshToken token = authRepository.find(refreshToken);
        if (token == null)
            return null;

        String newToken = tokenGenerator.generate();
        authRepository.refresh(token.token, newToken, LocalDate.now().plusDays(refreshTokenExpiry));

        String accessToken = jwtTokenProvider.createToken(token.userId);

        return new AbstractMap.SimpleEntry<>(accessToken, newToken);
    }

    @Override
    public void logout(long userId) {
        authRepository.remove(userId);
    }

    static class TokenGenerator {
        private static final String symbols = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789-_[]{}=";
        private static final int length = 50;
        private final Random rand;

        TokenGenerator() {
            rand = new Random();
        }

        public String generate() {
            StringBuilder res = new StringBuilder();
            for (int i = 0; i < length; i++) {
                res.append(symbols.charAt(rand.nextInt(symbols.length())));
            }
            return res.toString();
        }
    }
}
