package io.mirari.app.auth;

import java.time.LocalDate;

public class RefreshToken {

    public long userId;
    public String token;
    public LocalDate expiryDate;

    public RefreshToken(long userId, String token, LocalDate expiryDate) {
        this.userId = userId;
        this.token = token;
        this.expiryDate = expiryDate;
    }
}
