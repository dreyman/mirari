package io.mirari.app.repository;

import io.mirari.app.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    JdbcTemplate jdbc;
    RowMapper<User> mapper;

    @Autowired
    public UserRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbc = jdbcTemplate;
        mapper = new UserRowMapper();
    }

    @Override
    public long save(User user) {
        var q = "INSERT INTO users (username, password, created_at) values (?, ?, ?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbc.update(connection -> {
            PreparedStatement ps =
                    connection.prepareStatement(q, new String[]{"id"});
            ps.setString(1, user.username);
            ps.setString(2, user.password);
            ps.setTimestamp(3, Timestamp.valueOf(user.createdAt));
            return ps;
        }, keyHolder);
        long id = keyHolder.getKey().longValue();
        return id;
    }

    public User findByUsername(String username) {
        var q = "SELECT * FROM users WHERE username = ?";
        List<User> res = jdbc.query(q, mapper, username);
        return res.isEmpty() ? null : res.get(0);
    }

    @Override
    public User findById(long id) {
        var q = "SELECT * FROM users WHERE id = ?";
        List<User> res = jdbc.query(q, mapper, id);
        return res.isEmpty() ? null : res.get(0);
    }

    public boolean existsByUsername(String username) {
        var q = "SELECT COUNT(id) > 0 FROM users WHERE username = ?";
        var res = jdbc.queryForObject(q, Boolean.class, username);
        return res == null ? false : res;
    }

    static class UserRowMapper implements RowMapper<User> {
        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User u = new User();
            u.id = rs.getLong("id");
            u.username = rs.getString("username");
            u.password = rs.getString("password");
            u.createdAt = rs.getTimestamp("created_at").toLocalDateTime();
            return u;
        }
    }
}
