package io.mirari.app.repository;

import io.mirari.app.model.Note;
import org.postgresql.util.PGobject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@Repository
public class NoteRepositoryImpl implements NoteRepository {

    JdbcTemplate jdbc;
    RowMapper<Note> mapper;

    @Autowired
    public NoteRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbc = jdbcTemplate;
        this.mapper = new NoteRowMapper();
    }

    public List<Note> findAll(long userId) {
        var q = "SELECT * from note WHERE user_id = ? ORDER BY created_at DESC";
        return jdbc.query(q, mapper, userId);
    }

    @Override
    public long save(Note note) {
        var q = "INSERT INTO note (user_id, content, props, created_at) values (?, ?, ?, ?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbc.update(connection -> {
            PreparedStatement ps =
                    connection.prepareStatement(q, new String[]{"id"});
            ps.setLong(1, note.userId);
            ps.setString(2, note.content);
            PGobject propsJson = new PGobject();
            propsJson.setType("jsonb");
            propsJson.setValue(note.props);
            ps.setObject(3, propsJson);
            ps.setTimestamp(4, Timestamp.valueOf(note.createdAt));
            return ps;
        }, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public int update(Note note, long userId) {
        var q = "UPDATE note SET content=?, props=? WHERE id=? AND user_id = ?";
        PGobject propsJson = new PGobject();
        propsJson.setType("jsonb");
        try {
            propsJson.setValue(note.props);
        } catch (SQLException e) {
            throw new RuntimeException("Invalid json");
        }
        return jdbc.update(q, note.content, propsJson, note.id, userId);
    }

    @Override
    public int updateProps(long noteId, String props, long userId) {
        var q = "UPDATE note SET props=? WHERE id=? AND user_id=?";
        PGobject propsJson = new PGobject();
        propsJson.setType("jsonb");
        try {
            propsJson.setValue(props);
        } catch (SQLException e) {
            throw new RuntimeException("Invalid json");
        }
        return jdbc.update(q, propsJson, noteId, userId);
    }

    @Override
    public int delete(long id, long userId) {
        var q = "DELETE FROM note WHERE id = ? AND user_id = ?";
        return jdbc.update(q, id, userId);    }

    @Override
    public int delete(List<Long> ids, long userId) {
        var q = "DELETE FROM note WHERE userId = ? AND id IN (?)";
        return jdbc.update(q, userId, ids);
    }

    @Override
    public int clearFolder(long folderId, long userId) {
        var q = "UPDATE note SET props = props::jsonb - 'folder' " +
                "WHERE props::jsonb->'folder' = '" + folderId + "' " +
                " AND user_id = ?";
        return jdbc.update(q, userId);
    }

    @Override
    public int deleteByFolder(long folderId, long userId) {
        var q = "DELETE FROM note WHERE user_id = ? AND props::jsonb->'folder' = '" + folderId + "'";
        return jdbc.update(q, userId);
    }

    static class NoteRowMapper implements RowMapper<Note> {
        @Override
        public Note mapRow(ResultSet rs, int rowNum) throws SQLException {
            Note note = new Note();
            note.id = rs.getLong("id");
            note.userId = rs.getLong("user_id");
            note.content = rs.getString("content");
            note.props = rs.getString("props");
            note.createdAt = rs.getTimestamp("created_at").toLocalDateTime();
            return note;
        }
    }
}
