package io.mirari.app.repository;

import io.mirari.app.model.User;

public interface UserRepository {

    long save(User user);

    User findByUsername(String username);

    User findById(long id);

    boolean existsByUsername(String username);
}
