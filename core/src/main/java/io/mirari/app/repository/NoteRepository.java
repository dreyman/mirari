package io.mirari.app.repository;

import io.mirari.app.model.Note;

import java.util.List;

public interface NoteRepository {

    List<Note> findAll(long userId);

    long save(Note note);

    int update(Note note, long userId);

    int updateProps(long noteId, String props, long userId);

    int delete(long id, long userId);

    int delete(List<Long> ids, long userId);

    /**
     * Removes props.folder from all notes where note.props.folder == folderId.
     *
     * @param folderId id of the folder to clear
     * @param userId user id
     * @return number of updated rows
     */
    int clearFolder(long folderId, long userId);

    int deleteByFolder(long folderId, long userId);
}
