package io.mirari.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MirariApplication {

	public static void main(String[] args) {
		SpringApplication.run(MirariApplication.class, args);
	}

}
