package io.mirari.app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.mirari.app.auth.AuthRepository;
import io.mirari.app.auth.RefreshToken;
import io.mirari.app.controller.dto.AuthResponse;
import io.mirari.app.controller.dto.LoginRequest;
import io.mirari.app.model.User;
import io.mirari.app.repository.UserRepository;
import io.mirari.app.security.JwtTokenProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
@AutoConfigureMockMvc
@ComponentScan(basePackages = "io.mirari.app.*", lazyInit = true)
public class AuthControllerTest {

    @Autowired MockMvc mvc;
    @Autowired JwtTokenProvider jwtTokenProvider;
    @Autowired AuthController authController;

    @MockBean UserRepository userRepository;
    @MockBean AuthRepository authRepository;

    private User user;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void createUser() {
        user = new User();
        user.username = "testuser";
        user.password = "$2a$10$8GgFiJOX0KyD/8IK0MFdpuV8v1pkhLhWzDFuYr997rD6SJuOycpCy";
        user.id = 1;
    }

    @Test
    public void userCanSuccessfullyLogin() throws Exception {
        when(userRepository.findByUsername("testuser")).thenReturn(user);

        String resp = mvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"testuser\",\"password\": \"password\"}"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        var authToken = objectMapper.readValue(resp, AuthResponse.class);

        verify(authRepository, times(1)).save(any());
        assertTrue(authToken.accessToken.length() > 0);
        assertTrue(authToken.refreshToken.length() > 0);
    }

    @Test
    public void userGet401OnInvalidCredentials() throws Exception {
        when(userRepository.findByUsername("INVALID")).thenReturn(null);

        mvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"INVALID\",\"password\": \"INVALID\"}"))
                .andExpect(status().is(401));
    }

    @Test
    public void userReceives403OnExpiredToken() throws Exception {
        when(userRepository.findByUsername("testuser")).thenReturn(user);
        jwtTokenProvider.setExpiresIn(1);

        var token = (AuthResponse) authController.login(new LoginRequest("testuser", "password")).getBody();

        mvc.perform(get("/api/notes")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token.accessToken)
        ).andExpect(status().is(403));
    }

    @Test
    public void userCanGetNewAccessTokenUsingRefreshToken() throws Exception {
        when(authRepository.find("refresh_token_1")).thenReturn(new RefreshToken(user.id, "refresh_token_1", any()));

        String resp = mvc.perform(post("/auth/refresh")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"refreshToken\":\"refresh_token_1\"}"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        var auth = objectMapper.readValue(resp, AuthResponse.class);

        assertTrue(auth.accessToken.length() > 0);
        assertTrue(auth.refreshToken.length() > 0);
    }

    @Test
    public void resp404OnInvalidRefreshToken() throws Exception {
        when(authRepository.find("INVALID_REFRESH_TOKEN")).thenReturn(null);

        mvc.perform(post("/auth/refresh")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"refreshToken\":\"INVALID_REFRESH_TOKEN\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void userCanUseRefreshTokenOnlyOnce() throws Exception {
        when(authRepository.find("refresh_token")).thenReturn(new RefreshToken(user.id, "refresh_token", any()));

        String resp = mvc.perform(post("/auth/refresh")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"refreshToken\":\"refresh_token\"}"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        var auth = objectMapper.readValue(resp, AuthResponse.class);

        assertTrue(auth.accessToken.length() > 0);
        assertTrue(auth.refreshToken.length() > 0);

        when(authRepository.find("refresh_token")).thenReturn(null);

        mvc.perform(post("/auth/refresh")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"refreshToken\":\"refresh_token\"}"))
                .andExpect(status().isNotFound());
    }

}
